﻿using System.Diagnostics;
using Entities;
using System.Linq;
using System.Web.Services;

namespace Web
{
    /// <summary>
    /// Summary description for Server
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Server : WebService
    {

        [WebMethod]
        public Row SumMatrices(Row a, Row b)
        {
            // EventLog.WriteEntry("Threading", "Sum invoked remotely.");
            // Validate the same row.
            if (a.ID != b.ID) return null;
            // Validate the same row' size
            return a.Values.Count != b.Values.Count ? 
                null : 
                new Row {ID = a.ID, Values = a.Values.Select((x, index) => x + b.Values[index]).ToList()};
        }

        [WebMethod]
        public int MultiplyMatrices(int[] a, int[] b)
        {
            // EventLog.WriteEntry("Threading", "Multiplication invoked remotely.");
            return a.Select((value, index) => value*b[index]).Sum();
        }
    }
}
