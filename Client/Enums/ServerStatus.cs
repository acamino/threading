﻿namespace Client.Enums
{
    public enum ServerStatus
    {
        Busy,
        Available,
        Dead
    }
}