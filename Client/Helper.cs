﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Entities;
using Row = Client.Server.Row;


namespace Client
{
    public static class Helper
    {
        /// <summary>
        /// Generates a matrix (n, m).
        /// </summary>
        /// <param name="n">The number of rows.</param>
        /// <param name="m">The number of columns.</param>
        /// <returns>A list that contains the <see cref="Client.Server.Row"/> generated.</returns>
        public static List<Row> GenerateMatrix(int n, int m)
        {
            var matrix = new List<Row>();
            var random = new Random();
            
            for (var index = 0; index < n; index++)
            {
                var values = new int[m];
                for (var columnIndex = 0; columnIndex < m; columnIndex++)
                {
                    values[columnIndex] = random.Next(0, 10);
                }

                matrix.Add(new Row{ID = index, Values = values});
            }

            return matrix;
        }

        public static void Print(IEnumerable<Row> matrix)
        {
            Console.WriteLine();
            foreach (var row in matrix)
            {
                var values = new StringBuilder();
                foreach (var value in row.Values)
                {
                    values.AppendFormat("{0, 3} ", value);
                }
                Console.WriteLine("{0, 2} : [ {1}]", row.ID, values);
            }
        }

        /// <summary>
        /// Build a queue to process the multiplication in paralell.
        /// </summary>
        /// <param name="a">The matrix a.</param>
        /// <param name="b">The matrix b.</param>
        /// <returns>A queue with the positions and values for operation.</returns>
        public static Queue<MultiplicationItem> BuildQueueForMultiplication(IList<Row> a, IList<Row> b)
        {
            var postitions = BuildPositions(a.Count);

            var queue = new Queue<MultiplicationItem>();

            foreach (var item in postitions.Select(postition => new MultiplicationItem
                {
                    Position = postition,
                    Row = a[postition.RowId].Values,
                    Column = b.Select(row => row.Values[postition.ColumnId]).ToArray()
                }))
            {
                queue.Enqueue(item);
            }

            return queue;
        }

        /// <summary>
        /// Build the multiplication result matrix.
        /// </summary>
        /// <param name="data">The dictionary with the position and its result.</param>
        /// <returns>A collection with the result matrix.</returns>
        public static IEnumerable<Row> BuildMultiplicationResultMatrix(IDictionary<Position, int> data)
        {
            var size = Math.Sqrt(data.Count);

            var result = new List<Row>();

            for (var index = 0; index < size; index++)
            {
                var internalIndex = index;
                var rowElements = data.Where(x => x.Key.RowId == internalIndex);
                var row = new Row {ID = index};
                var values = new List<int>();
                for (var innerIndex = 0; innerIndex < size; innerIndex++)
                {
                    var keyValuePairs = rowElements as KeyValuePair<Position, int>[] ?? rowElements.ToArray();
                    var value = keyValuePairs.FirstOrDefault(x => x.Key.ColumnId == innerIndex).Value;
                    values.Add(value);
                }
                row.Values = values.ToArray();

                result.Add(row);
            }

            return result;
        }

        /// <summary>
        /// Build a grid of positions for paralell processing.
        /// </summary>
        /// <param name="size">The size of the matrix.</param>
        /// <returns>A collection of positions.</returns>
        private static IEnumerable<Position> BuildPositions(int size)
        {
            var positions = new List<Position>();

            // Generated the positions.
            for (var index = 0; index < size; index++)
            {
                for (var innerIndex = 0; innerIndex < size; innerIndex++)
                {
                    positions.Add(new Position { RowId = index, ColumnId = innerIndex });
                }
            }

            return positions;
        }

        /// <summary>
        /// Check service availability.
        /// </summary>
        /// <param name="url">The URL</param>
        /// <returns>true if the server s available otherwise false.</returns>
        public static bool IsServerAvailable(string url)
        {
            try
            {
                var myRequest = (HttpWebRequest)WebRequest.Create(url);
                var response = (HttpWebResponse)myRequest.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }

                Console.WriteLine("{0} Returned, but with status: {1}", url, response.StatusDescription);
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} unavailable: {1}", url, ex.Message);
                return false;
            }
        }
    }
}
