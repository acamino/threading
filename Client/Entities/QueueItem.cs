﻿using Row = Client.Server.Row;

namespace Client.Entities
{
    /// <summary>
    /// Represents the item to be enqueued.
    /// </summary>
    public class QueueItem
    {
        /// <summary>
        /// Row from matrix a.
        /// </summary>
        public Row A { get; set; }

        /// <summary>
        /// Row from matrix b.
        /// </summary>
        public Row B { get; set; }
    }
}
