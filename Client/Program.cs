﻿/// Group: Camino . Iza . Tapia
using Client.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Client.Enums;


namespace Client
{
    class Program
    {
        static void Main()
        {
            // Set the operation for demo.
// ReSharper disable ConvertToConstant.Local
            var operation = Operation.Sum;
            // n means rows.
            var n = 10;
            // m means columns.
            var m = 10;
// ReSharper restore ConvertToConstant.Local

            var a = Helper.GenerateMatrix(n, m);
            // Added sleep time to prevent the generation of the same matrices.
            Thread.Sleep(100);
            var b = Helper.GenerateMatrix(n, m);

            var shouldProcess = true;
            // Initialize the stopwatch.
            var stopwatch = new Stopwatch();
            
            // Variable used to store all threads.
            var threads = new List<Thread>();

            switch (operation)
            {
                case Operation.Sum:
                    // Put the rows in a queue for processing.
                    var queue = new Queue<QueueItem>();
                    for (var index = 0; index < n; index++)
                    {
                        queue.Enqueue(new QueueItem { A = a[index], B = b[index] });
                    }

                    stopwatch.Start();

                    while (shouldProcess)
                    {
                        // Gets an instance of an available server.
                        var server = ServerWrapper.GetServer();
                        if (server == null) continue;

                        // This code will be excecuted only if an instance of server is available.
                        var data = queue.Dequeue();
                        var thread = new Thread(() => ServerWrapper.Sum(server, data));
                        thread.Start();

                        threads.Add(thread);

                        // The process should end when all the items in the queue are processed.
                        shouldProcess = queue.Count != 0;
                    }

                    // To display the results, wait until all the threads have finished.
                    foreach (var thread in threads)
                    {
                        thread.Join();
                    }

                    // Write results.
                    Console.WriteLine("Processed time: {0}", stopwatch.ElapsedMilliseconds);
                    Console.WriteLine("Available Servers: {0}", ServerWrapper.EndPoints.Count);

                    Helper.Print(a);
                    Helper.Print(b);

                    // Print the result.
                    Helper.Print(ServerWrapper.GetResult());
                    break;
                case Operation.Multiplication:
                    // For simplicity work with sqare matrices.
                    if (n != m)
                    {
                        Console.WriteLine("The matrices must be squares.");
                        return;
                    }

                    var multiplicationQueue = Helper.BuildQueueForMultiplication(a, b);

                    stopwatch.Start();

                    while (shouldProcess)
                    {
                        // Gets an instance of an available server.
                        var server = ServerWrapper.GetServer();
                        if (server == null) continue;

                        // This code will be excecuted only if an instance of server is available.
                        var data = multiplicationQueue.Dequeue();
                        var thread = new Thread(() => ServerWrapper.Multiply(server, data));
                        thread.Start();

                        threads.Add(thread);

                        // The process should end when all the items in the queue are processed.
                        shouldProcess = multiplicationQueue.Count != 0;
                    }

                    // To display the results, wait until all the threads have finished.
                    foreach (var thread in threads)
                    {
                        thread.Join();
                    }

                    // Write results.
                    Console.WriteLine("Processed time: {0}", stopwatch.ElapsedMilliseconds);
                    Console.WriteLine("Available Servers: {0}", ServerWrapper.EndPoints.Count);

                    Helper.Print(a);
                    Helper.Print(b);

                    // Print the result.
                    Helper.Print(ServerWrapper.GetMultiplicationResult());

                    break;
                default:
                    Console.WriteLine("Unsupported Operation");
                    break;

            }
            
            Console.Read();
        }
    }
}
