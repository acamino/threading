﻿using Client.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Entities;
using QueueItem = Client.Entities.QueueItem;
using Row = Client.Server.Row;

namespace Client
{
    class ServerWrapper
    {

        /// <summary>
        /// End points collection that keeps the server status <see cref="ServerStatus"/>
        /// </summary>
        public static IDictionary<string, ServerStatus> EndPoints = new Dictionary<string, ServerStatus>
            {
                // {"http://172.20.10.2/webservice/server.asmx", ServerStatus.Available},
                //{"http://threading.apphb.com/server.asmx", ServerStatus.Available},
                {"http://localhost:8085/webservice/server.asmx", ServerStatus.Available},
                {"http://localhost:80/webservice/server.asmx", ServerStatus.Available},
                {"http://localhost:8080/webservice/server.asmx", ServerStatus.Available},
                {"http://localhost:8081/webservice/server.asmx", ServerStatus.Available},
                {"http://localhost:8082/webservice/server.asmx", ServerStatus.Available},
                {"http://localhost:8083/webservice/server.asmx", ServerStatus.Available}
            };

        private static readonly IList<Row> Result = new List<Row>();
        private static readonly IDictionary<Position, int> MultiplicationResult = new Dictionary<Position, int>(); 

        /// <summary>
        /// Computes the sum in the server.
        /// </summary>
        /// <param name="server">The server.</param>
        /// <param name="data">The data.</param>
        public static void Sum(Server.Server server, QueueItem data)
        {
            var response = server.SumMatrices(data.A, data.B);
            if (response == null) return;
            lock (EndPoints)
            {
                Result.Add(response);
                Helper.Print(new List<Row> { response });
                Console.WriteLine(server.Url);
                EndPoints[server.Url] = ServerStatus.Available;
            }
        }

        /// <summary>
        /// Computes the multiplication in the server.
        /// </summary>
        /// <param name="server">The server.</param>
        /// <param name="data">The data.</param>
        public static void Multiply(Server.Server server, MultiplicationItem data)
        {
            var response = server.MultiplyMatrices(data.Row, data.Column);
            lock (EndPoints)
            {
                MultiplicationResult.Add(data.Position, response);
                Console.WriteLine(server.Url);
                EndPoints[server.Url] = ServerStatus.Available;
            }
        }

        /// <summary>
        /// Gets an available server.
        /// </summary>
        /// <returns>A server if there is an available one otherwise null.</returns>
        public static Server.Server GetServer()
        {
            // Used  locked to prevent other thread updates the EndPoints dictionary simultaneously.
            lock (EndPoints)
            {
                // Expression used to look for available endpoints.
                var endPoint = EndPoints.FirstOrDefault(x => x.Value == ServerStatus.Available);

                if (endPoint.Key != null)
                {
                    if (!Helper.IsServerAvailable(endPoint.Key))
                    {
                        // Set the end point as Dead.
                        EndPoints[endPoint.Key] = ServerStatus.Dead;
                        return null;
                    }

                    // Set the end point as Busy.
                    EndPoints[endPoint.Key] = ServerStatus.Busy;
                    return new Server.Server { Url = endPoint.Key };
                    
                }

                // If all servers are busy wait.
                Thread.Sleep(100);

                return null;
            }
        }

        /// <summary>
        /// Gets the matrix result.
        /// </summary>
        /// <returns>A collection of rows ordered by ID.</returns>
        public static IOrderedEnumerable<Row> GetResult()
        {
            return Result.OrderBy(x => x.ID);
        }

        /// <summary>
        /// Gets the matrix result.
        /// </summary>
        /// <returns>A collection of rows.</returns>
        public static IEnumerable<Row> GetMultiplicationResult()
        {
            return Helper.BuildMultiplicationResultMatrix(MultiplicationResult);
        }
    }
}
