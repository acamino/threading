﻿using System.Collections.Generic;

namespace Entities
{
    public class Row
    {
        public int ID { get; set; }
        public Position Position { get; set; }
        public List<int> Values { get; set; }
    }
}
