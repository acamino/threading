﻿namespace Entities
{
    public class MultiplicationItem
    {
        public Position Position { get; set; }
        public int[] Row { get; set; }
        public int[] Column { get; set; }
    }
}
