﻿namespace Entities
{
    public class Position
    {
        public int RowId { get; set; }
        public int ColumnId { get; set; }
    }
}
